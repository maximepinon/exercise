let app = require('express')()
let bodyParser = require('body-parser')
let unirest = require('unirest')
let mongoose = require("mongoose")
let config = require('config');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


/* Promocode Model Part */
mongoose.Promise = global.Promise;mongoose.connect("mongodb://localhost:27017/promocode")
var promocodeSchema = new mongoose.Schema({
    name: { type : String , unique : true, required : true},
    avantage: { type : Number , required : true},
    restriction: { type : String , required : true},
   });

var Promocode = mongoose.model("Promocode", promocodeSchema)


/* Routes Part */

/* User asking for a promotion */
app.post('/promotion', (req, res) => {
    var promotionName = req.body.promocode_name 
    var age = req.body.arguments.age
    var town = req.body.arguments.meteo.town

    unirest.get(`http://api.openweathermap.org/data/2.5/find?q=`+town+`,fr&units=metric&appid=${config.get('tokenAPI')}`)
                .end(function(res) {
                    if (res.error) {
                        res.status(400).send('GET error', res.error)
                    } else {
                        var temp = res.body.list[0].main.temp
                        var weather = res.body.list[0].weather[0].main
                        //function to check if user can have the promotion
                        res.send('Success')
                    }
                })
    
})

/* Add a new promocode */
app.post('/promocode', (req, res) => {
    var myData = new Promocode(req.body)
    myData.save()
    .then(item => {
    res.send("item saved to database")
    })
    .catch(err => {
    res.status(400).send("unable to save to database")
    });
})


/* Get a promocode */
app.get('/promocode/:name', (req, res) => {
    Promocode.findOne({ name: req.params.name}, function (err, promocode) {
        res.json(promocode)
    });
})


app.listen(8080)
console.log("Starting at port 8080")